<?php
Route::get('/refresh',function(){
return redirect('/');
});


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/create-feedback','HomeController@getCreateFeedback');
Route::get('/view-feedback/{fid}','HomeController@getViewFeedback');
Route::get('/view-report/{fid}','HomeController@getViewReport');

Route::post('/users/update/{id}','HomeController@postUpdateUsers');

Route::post('/create-feedback','HomeController@postCreateFeedback');
Route::post('/delete-feedback/{fid}','HomeController@postDeleteFeedback');

Route::post('/add-question','HomeController@postAddQuestion');
Route::post('/answer','HomeController@postAnswer');