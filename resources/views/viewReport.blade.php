@extends('layouts.app')

@section('content')

    @include('sidebar')

    <div class="pusher">

        <div class="ui stackable menu">
            <div class="item">
                <img src="{{url('/images/logo.png')}}">
            </div>


            <div class="right menu">

                <a class="item" >Logged in as {{Auth::user()->name}}</a>
            </div>
        </div>


        <div class="ui container grid centered">
            <div class="sixteen wide column">


                <div class="ui container grid centered" >

                    <div align="center" class="feedbackHeader" style="margin-top: 20px">
                        <h3>{{$feedback->title}}</h3>

                            <h5>{{$feedback->description}}</h5>

                    </div>

                    <div class="ui row">
                        <div class="ui four column">
                            {{count($feedback->Questions)}} Total Questions
                        </div>

                        <div class="ui four column">
                            {{count($responses)}} Total Responses
                        </div>
                    </div>

                    <table class="ui celled table">

                        <thead>
                        <tr>
                            <th>Question</th>
                            <th>Yes</th>
                            <th>No</th>
                            @if(Auth::user()->role == "Student")
                                <th></th>
                            @else
                                <th>Created At</th>
                            @endif
                        </tr>
                        </thead>

                        <tbody>
                            @foreach($feedback->Questions as $item)
                                <tr>
                                    <td>
                                        {{$item->question}}
                                    </td>
                                    <td>
                                        <?php
                                        $resps = $item->Response;
                                            $count = 0;
                                            foreach($resps as $i){
                                                if($i){
                                                    $count++;
                                                }
                                            }
                                            echo $count;
                                        ?>

                                    </td>
                                    <td>
                                        {{ count($responses) - $count}}

                                    </td>
                                    <td>
                                        {{$item->created_at}}
                                    </td>
                                </tr>

                            @endforeach
                        </tbody>

                    </table>

                </div>

                </div>
        </div>
    </div>

@endsection