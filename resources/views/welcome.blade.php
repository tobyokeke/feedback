@extends('layouts.app')

@section('content')

@include('sidebar')

    <div class="pusher">

        <div class="ui stackable menu">
            <div class="item">
                <img src="{{url('/images/logo.png')}}">
            </div>


            <div class="right menu">

                <a class="item" >Logged in as {{Auth::user()->name}}</a>
            </div>
        </div>


        <div class="ui container grid centered">
            <div class="sixteen wide column">

                @if( Session::has('status') )
                    <div class="green" align="center">{{ Session::get('status')}}</div>
                @endif


            @if(Auth::user()->role == "Admin")

                    <table class="ui celled table">
                        <thead>
                        <tr><th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th></th>
                            <th></th>
                        </tr></thead>
                        <tbody>

                        @foreach($users as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->role}}</td>
                            <td>
                                <form method="post" action="{{url('/users/update/' . $item->uid)}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                    <select name="newRole">
                                        <option value="Student">Student</option>
                                        <option value="Admin">Admin</option>
                                        <option value="Feedback Analyst">Feedback Analyst</option>
                                    </select>
                                    <button class="ui basic positive button" type="submit">Change</button>

                                </form>


                            </td>

                            <td>
                                <form method="post" action="{{url('/user/delete/'. $item->uid)}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                    <button class="ui negative basic button" type="submit">Delete</button>
                                </form>

                            </td>

                        </tr>

                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr><th colspan="5">
                                <div class="ui right floated pagination menu">
                                    <a class="icon item">
                                        <i class="left chevron icon"></i>
                                    </a>
                                    <a class="item">1</a>
                                    <a class="item">2</a>
                                    <a class="item">3</a>
                                    <a class="item">4</a>
                                    <a class="icon item">
                                        <i class="right chevron icon"></i>
                                    </a>
                                </div>
                            </th>
                        </tr></tfoot>
                    </table>

            @endif



            @if(Auth::user()->role == "Feedback Analyst")

                <table class="ui celled table">
                    <thead>
                    <tr><th>Title</th>
                        <th>Description</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr></thead>
                    <tbody>

                    @foreach($feedback as $item)
                        <tr>
                            <td>{{$item->title}}</td>
                            <td>{{$item->description}}</td>
                            <td>
                                <a class="ui basic positive button" href="{{url('/view-feedback/' . $item->fid)}}" >View</a>

                            </td>

                            <td>
                                <a class="ui grey button" href="{{url('/view-report/' . $item->fid)}}" >Reports</a>
                            </td>

                            <td>

                                <form method="post" action="{{url('/delete-feedback/' .$item->fid)}}">
                                    {{csrf_field()}}
                                    <button class="ui basic negative button" type="submit" >Delete</button>
                                </form>

                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr><th colspan="5">
                            <div class="ui right floated pagination menu">
                                <a class="icon item">
                                    <i class="left chevron icon"></i>
                                </a>
                                <a class="item">1</a>
                                <a class="item">2</a>
                                <a class="item">3</a>
                                <a class="item">4</a>
                                <a class="icon item">
                                    <i class="right chevron icon"></i>
                                </a>
                            </div>
                        </th>
                    </tr></tfoot>
                </table>

            @endif



            @if(Auth::user()->role == "Student")

                <table class="ui celled table">
                    <thead>
                        <tr><th>Title</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    @if(count($feedback) <= 0)
                        <tr align="center">
                            <td colspan="3">
                                No Pending Feedback
                            </td>
                        </tr>
                    @endif

                    @foreach($feedback as $item)
                        <tr>
                            <td>{{$item->title}}</td>
                            <td>{{$item->description}}</td>
                            <td>
                                <a class="ui basic positive button" href="{{url('/view-feedback/' . $item->fid)}}" >View</a>

                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr><th colspan="5">
                            <div class="ui right floated pagination menu">
                                <a class="icon item">
                                    <i class="left chevron icon"></i>
                                </a>
                                <a class="item">1</a>
                                <a class="item">2</a>
                                <a class="item">3</a>
                                <a class="item">4</a>
                                <a class="icon item">
                                    <i class="right chevron icon"></i>
                                </a>
                            </div>
                        </th>
                    </tr></tfoot>
                </table>

            @endif





            </div>




        </div>
    </div>

@endsection