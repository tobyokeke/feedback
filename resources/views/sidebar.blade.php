<div class="ui sidebar visible inverted vertical menu">

    <a class="item sidebarProfileName" href="{{url('/')}}">
        {{--{{Auth::user()->name}}--}}
        <img  src="{{url('/images/logo.png')}}">

       <span id="regentTitle">REGENT FEEDBACK PORTAL</span>
    </a>



    @if(Auth::guest())

        <a href='{{url('/login')}}' class="item">
            Sign In
        </a>
        <a href="{{url('/register')}}" class="item">
            Sign Up
        </a>

    @else

        @if(Auth::user()->role == "Student")
            <a class="item" href="{{url('/')}}">
                Pending Feedback
            </a>

        @endif

        @if(Auth::user()->role == "Admin")

                <a class="item">
                    Manage Users
                </a>

        @endif

        @if(Auth::user()->role == "Feedback Analyst")
                <a class="item" href="{{url('/create-feedback')}}">
                    Create Feedback
                </a>
                <a class="item" href="{{url('/')}}">
                    Manage Feedbacks
                </a>

        @endif




        <a
           onclick="event.preventDefault();
           document.getElementById('logout-form').submit();"
           class="item" >
            Sign out
        </a>


    @endif

</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>

