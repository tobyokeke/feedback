@extends('layouts.app')

@section('content')

    @include('sidebar')


    <div class="pusher">
        <div class="ui stackable menu">
            <div class="item">
                <img src="{{url('/images/logo.png')}}">
            </div>


            <div class="right menu">

                <a class="item" >Logged in as {{Auth::user()->name}}</a>
            </div>
        </div>

        @if( Session::has('status') )
            <div class="ui green message" align="center">{{ Session::get('status')}}</div>
        @endif

        <div class="ui container grid centered" >

            <div align="center" class="feedbackHeader" style="margin-top: 20px">
                <h3>{{$feedback->title}}</h3>
                @if(Auth::user()->role != "Student")
                <h5>{{$feedback->description}}</h5>
                @endif
            </div>
            <table class="ui celled table">

                <thead>
                    <tr>
                        <th>Question</th>

                        @if(Auth::user()->role == "Student")
                            <th></th>
                        @else
                            <th>Created At</th>
                        @endif
                    </tr>
                </thead>

                @if(count($feedbackQuestions) <= 0)
                    <tr>
                        <td style="text-align: center" colspan="2">No Questions</td>
                    </tr>
                @endif
                  @foreach($feedbackQuestions as $item)
                    <tr>
                        <td>{{$item->question}}</td>

                        @if(Auth::user()->role == "Student")
                            <td>
                                <form class="ui form">

                                    <input type="hidden" value="{{$item->fqid}}" name="fqid">
                                    <div class="inline field">
                                        <div class="ui checkbox">
                                            <input  tabindex="0" class="yes hidden" type="checkbox">
                                            <label>Yes</label>
                                        </div>
                                        <div class="ui checkbox">
                                            <input tabindex="1" class=" no hidden" type="checkbox">
                                            <label>No</label>
                                        </div>
                                    </div>

                                </form>
                            </td>
                        @else
                            <td>{{$item->created_at}}</td>
                        @endif

                    </tr>
                @endforeach

            </table>

            @if(Auth::user()->role == "Feedback Analyst")

            <form method="post" class="ui form" action="{{url('/add-question')}}">

                {{csrf_field()}}
                <input type="hidden" name="fid" value="{{$feedback->fid}}">
                <label for="question">Question</label>
                <textarea  id="question" class="ui input" name="question"></textarea>
                <button  style="margin-top: 20px;" class="ui green button right" type="submit" id="addQuestion">Add</button>
            </form>

            @endif

            @if(Auth::user()->role == "Student")
                <button class="ui green button" id="submitButton">Submit</button>
            @endif

        </div>


    </div>

    <script>
        $('.ui.checkbox')
                .checkbox()
        ;

        $(document).ready(function () {

            $('#submitButton').click(function () {
                var forms = $("form");

                for(var i =0; i < forms.length; i++){
                    var fqid,yes,no,response;
                    fqid = $(forms[i][0]).val();

                    // yes answers array
                    $(forms[i][1]).each(function(){
                        yes = this.checked;

                    });

                    // yes answers array
                    $(forms[i][2]).each(function(){
                        no = this.checked;

                    });

                    console.log(yes + " | " + no + " | " + fqid );

                    if(yes == true)
                        response = 1;
                    else if (no == true )
                        response = 0;
                    else
                        response = 0;



                    $.ajax({
                        url:"{{url('/answer')}}",
                        method: "post",
                        data:{_token:"{{csrf_token()}}", fqid:fqid, answer: response  }
                    });


                } // end for


                setTimeout(window.location = "{{url('/refresh')}}", 1000);


            });


        })
    </script>
@endsection