@extends('layouts.app')

@section('content')

@include('sidebar')
    <div class="ui container grid centered" style="margin-left: 200px !important;">


        <div id="loginBody" class="ui attached segment">
            <div id="loginHeader" class="ui top attached header">Sign In</div>

            <form class="ui form" role="form" method="POST" action="{{ url('/login') }}">
                {!! csrf_field() !!}

                <div class="field form-group{{ $errors->has('email') ? ' has-error' : '' }}">


                    <div class="col-md-6">
                        <div class="ui labeled input">
                            <div class="ui label">
                                Email:
                            </div>
                            <input placeholder="Your email" type="email"  class="ui input" name="email" value="{{ old('email') }}">
                        </div>


                        @if ($errors->has('email'))

                            <div class="ui pointing red basic label">
                                {{ $errors->first('email') }}
                            </div>

                        @endif
                    </div>
                </div>

                <div class="field form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                    <div class="ui labeled input">
                        <div class="ui label" >
                            Password:
                        </div>
                        <input placeholder="Your password" type="password"  class="ui input" name="password">
                    </div>


                    @if ($errors->has('password'))

                        <div class="ui pointing red basic label">
                            {{ $errors->first('password') }}
                        </div>

                    @endif

                </div>

                <div class="field">

                    <div class="ui checkbox">
                        <input class="hidden" tabindex="0" type="checkbox" name="remember">
                        <label>Remember Me</label>
                    </div>

                </div>

                <div class="field">
                    <div class="inline field">
                        <button type="submit" class="ui green button">
                            <i class="sign in icon"></i>Login
                        </button>

                        <a class="link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                    </div>
                </div>
            </form>

        </div>

    </div>

@endsection