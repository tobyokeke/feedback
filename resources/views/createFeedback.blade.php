@extends('layouts.app')

@section('content')

    @include('sidebar')


<div class="pusher">
    <div class="ui stackable menu">
        <div class="item">
            <img src="{{url('/images/logo.png')}}">
        </div>


        <div class="right menu">

            <a class="item" >Logged in as {{Auth::user()->name}}</a>
        </div>
    </div>

    @if( Session::has('status') )
        <div class="ui green message" align="center">{{ Session::get('status')}}</div>
    @endif

    <div class="ui container grid centered" >


        <div id="feedback" class="ui attached segment">
            <div id="feedbackHeader" class="ui top attached header">Create Feedback</div>

            <form class="ui form" role="form" method="POST" action="{{ url('/create-feedback') }}">
                {!! csrf_field() !!}

                <div>

                    <div class="field">
                        <label>Title</label>
                        <input type="text" name="title">
                    </div>

                </div>

                <div >

                    <div class="field">
                        <label>Description</label>
                        <textarea name="description"></textarea>
                    </div>

                </div>
                <br>
                <div class="field">
                    <div class="inline field">
                        <button type="submit" class="ui green button">
                         Create
                        </button>

                        <button type="reset" class="ui red button">
                                Clear
                        </button>
                    </div>


                </div>
            </form>

        </div>

    </div>
</div>
@endsection