<?php

namespace App\Http\Controllers;

use App\answered;
use App\feedback;
use App\feedbackquestions;
use App\responses;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $users = User::all();
	    $feedback = feedback::all()->sortByDesc('created_at');

	    if(Auth::user()->role == "Student"){

		    $answered = answered::where('uid',Auth::user()->uid)->get();

		    $selected = array();


		    foreach($feedback as $feed){

			    foreach($feed->Questions as $item){
					foreach($answered as $answer){
						if($item->fqid == $answer->fqid){
							array_push($selected,$item->Feedback->fid);
						}
					}
	    	    }
		    }


		    $answered = feedback::whereIn('fid',$selected)->get();

		    $feedback = feedback::all()->diff($answered);

	    }

        return view('welcome',[
	        'users' => $users,
	        'feedback' => $feedback
        ]);
    }

	public function getCreateFeedback() {
		return view('createFeedback');
	}

	public function getViewReport( $fid ) {
		$feedback = feedback::find($fid);

		$responses = responses::whereIn('fqid',$feedback->Questions)->get();

		return view('viewReport',[
			'feedback' => $feedback,
			'responses' => $responses
		]);
	}

	public function getViewFeedback($fid) {
		$feedback = feedback::find($fid);
		$feedbackQuestions = feedbackquestions::where('fid',$fid)->get();
		return view('viewFeedback',[
			'feedback' => $feedback,
			'feedbackQuestions' => $feedbackQuestions
		]);
	}

	public function postAddQuestion( Request $request ) {
		$fid = $request->input('fid');
		$question = new feedbackquestions();
		$question->question = $request->input('question');
		$question->fid = $fid;
		$question->save();

		$request->session()->flash('status' , 'Question Successfully Added');

		return redirect('/view-feedback/' . $fid);
	}

	public function postDeleteUser($id){
		$user = User::find($id);

		$user->delete();

		return redirect('/');

	}


	public function postUpdateUsers($id, Request $request){
		$user = User::find($id);
		$user->role = $request->input('newRole');
		$user->save();


		$request->session()->flash('status' , 'Successfully Updated role');
		return redirect('/');

	}

	public function postCreateFeedback( Request $request ) {
		$feedback = new feedback();
		$feedback->title = $request->input('title');
		$feedback->description = $request->input('description');
		$feedback->uid = Auth::user()->uid;
		$feedback->save();
		$request->session()->flash("status","Feedback Created");

		return redirect('/create-feedback');
	}

	public function postDeleteFeedback($fid, Request $request) {
		$feedback = feedback::find($fid);
		$feedback->delete();

		$request->session()->flash("status","Feedback Deleted Successfully");
		return redirect('/');
	}

	public function postAnswer(Request $request){
		$answer = new responses();
		$answer->fqid = $request->input('fqid');
		$answer->answer = $request->input('answer');
		$answer->save();

		$answered = new answered();
		$answered->fqid = $request->input('fqid');
		$answered->uid = Auth::user()->uid;
		$answered->save();


	}

}
