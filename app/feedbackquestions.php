<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class feedbackquestions extends Model
{

	protected $primaryKey = 'fqid';
	public function Feedback(){
		return $this->belongsTo('App\feedback','fid','fid');
	}

	public function Response(){
		return $this->hasMany('App\responses','fqid','fqid');
	}
}
