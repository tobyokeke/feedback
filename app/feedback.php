<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class feedback extends Model
{
    protected $primaryKey = 'fid';

	public function Questions(){
		return $this->hasMany('App\feedbackquestions','fid','fid');
	}
}
