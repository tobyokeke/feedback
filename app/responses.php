<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class responses extends Model
{
    public $table = 'responses';
	protected $primaryKey = 'resid';

	public function FeedbackQuestion(){
		return $this->hasOne('App\feedbackquestion','fqid','fqid');
	}
}
